variable "appId" {
  description = "Azure Kubernetes Service Cluster service principal"
}

variable "password" {
  description = "Azure Kubernetes Service Cluster password"
}

//variable "subscription_id" {
//  description = "Subscription ID"
//}

//variable "tenant_id" {
//  description = "Tenant ID"
//}

variable node_min {
  default = 1
  type = number
  description = "minmum number in node pool"
}

variable node_max {
  default = 8
  type = number 
  description = "maxmum number in node pool"
}

variable node_pod {
  type = number 
  description = "maxmum number in node pool"
}

variable node_asp {
  type = number
}




