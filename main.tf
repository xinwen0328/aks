//terraform {
//  required_providers {
//    azurerm = {
//      source  = "hashicorp/azurerm"
//      version = "2.66.0"
//    }
//  }
//
//  required_version = ">= 0.14"
//}

//provider "azurerm" {
//  features {}

//  subscription_id             = var.subscription_id
//  client_id                   = var.appId
//  client_secret               = var.password
//  tenant_id                   = var.tenant_id
//}

resource "random_pet" "prefix" {}

resource "azurerm_resource_group" "default" {
  name     = "${random_pet.prefix.id}-rg"
  location = "West US 2"

  tags = {
    environment = "Demo"
  }
}



resource "azurerm_kubernetes_cluster" "default" {
  name                = "${random_pet.prefix.id}-aks"
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  dns_prefix          = "${random_pet.prefix.id}-k8s"

  auto_scaler_profile {
    scale_down_utilization_threshold = var.node_asp
   }

  default_node_pool {
    name            = "default"
    node_count      = 1
    vm_size         = "Standard_B2s"
    os_disk_size_gb = 30
    enable_auto_scaling = true
    min_count = var.node_min
    max_pods = var.node_pod
    max_count = var.node_max

  }

  service_principal {
    client_id     = var.appId
    client_secret = var.password
  }

  role_based_access_control {
    enabled = true
  }

  tags = {
    environment = "Demo"
    iac_version = "v0.8"

  }
}
